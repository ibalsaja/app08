package lana.ahmad.myapplication.app08

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener {

    var background: String = ""
    var selectedBack : String = ""
    var currentBack = ""
    var checkHD = ""
    var getTitle = ""

    lateinit var adapterSpin : ArrayAdapter<String>
    val arrayBackground = arrayOf("Blue","Yellow","Green","Black")

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val bgMain = "background"
    val DEF_BACK = "White"
    val bgHeader = "header"
    var checkedRadio = ""
    val TITLE_FILM = "SPACE BETWEEN US"
    val CONTENT_FILM = "Content Film"
    val DEFF_CONTENT_FILM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum ut felis tincidunt, iaculis elit sed, venenatis mi. "
    val DEFF_FILM = "SPACE BETWEEN US"
    val FONT_SUBTITLE = "subFont"
    val DEF_FONT_SIZE = 20
    val FONT_DETAIL = "F"

    override fun onClick(v: View?) {
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val prefEditor = preferences.edit()
        prefEditor.putString(bgMain,selectedBack)
        prefEditor.putString(bgHeader,checkedRadio)
        prefEditor.putString(TITLE_FILM,edTitle.text.toString())
        prefEditor.putInt(FONT_SUBTITLE,sbHead.progress)
        prefEditor.putString(CONTENT_FILM,edIsi.text.toString())
        prefEditor.putInt(FONT_DETAIL,sbTitle.progress)
        prefEditor.commit()
        System.exit(0)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        currentBack = preferences.getString(bgMain,DEF_BACK).toString()
        adapterSpin = ArrayAdapter(this,android.R.layout.simple_list_item_1,arrayBackground)
        checkHD = preferences.getString(bgHeader,DEF_BACK).toString()
        edTitle.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        edIsi.setText(preferences.getString(CONTENT_FILM,DEFF_CONTENT_FILM))
        sbHead.progress = preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE)
        sbTitle.progress = preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE)
        spBg.adapter = adapterSpin
        spBg.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                background = adapterSpin.getItem(position).toString()
                if(background.equals("Blue")){
                    selectedBack = "Blue"
                }
                if(background.equals("Yellow")){
                    selectedBack = "Yellow"
                }
                if(background.equals("Green")){
                    selectedBack = "Green"
                }
                if(background.equals("Black")){
                    selectedBack = "Black"
                }
            }
        }

        rg.setOnCheckedChangeListener{group, checkedId ->
            when(checkedId){
                R.id.rb1->{
                    checkedRadio = "Blue"
                }R.id.rb2->{
                checkedRadio = "Yellow"
            }R.id.rb3->{
                checkedRadio = "Green"
            }R.id.rb4->{
                checkedRadio = "Black"
            }
            }
        }

        btnSimpan.setOnClickListener(this)
    }



    override fun onStart() {
        super.onStart()
        spBg.setSelection(getIndex(spBg,currentBack))
        getChecked()
    }

    fun getIndex(spinner: Spinner, myString: String) : Int {
        var a = spinner.count
        var b : String =""
        for(i in 0 until a){
            b = arrayBackground.get(i)
            if(b.equals(myString,ignoreCase = true)){
                return  i
            }
        }
        return 0
    }

    fun getChecked(){
        if(checkHD.equals("Blue")){
            rb1.isChecked = true
        }
        if(checkHD.equals("Green")){
            rb2.isChecked = true
        }
        if(checkHD.equals("Yellow")){
            rb3.isChecked = true
        }
        if(checkHD.equals("Black")){
            rb4.isChecked = true
        }
    }
}
