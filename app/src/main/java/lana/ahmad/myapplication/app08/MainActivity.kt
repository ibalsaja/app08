package lana.ahmad.myapplication.app08

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val FONT_SUBTITLE = "subFont"
    val FONT_DETAIL = "F"
    val DEF_FONT_SIZE = 20
    val DEF_TEXT = "Hello World"
    val bgHeader = "header"
    val bgMain = "background"
    val DEF_BACK = "White"
    val TITLE_FILM = "space between us"
    val DEFF_FILM = "space between us"
    val CONTENT_FILM = "Content Film"
    val DEFF_CONTENT_FILM = "Semua tentang jarak,waktu dan tempat "

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var menuInflater = menuInflater
        menuInflater.inflate(R.menu.menu_main,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        val wholeBack = preferences.getString(bgMain,DEF_BACK).toString()
        getBackground(wholeBack,true)
        val bgHd = preferences.getString(bgHeader,DEF_BACK).toString()
        getBackground(bgHd,false)
        txTitle.setText(preferences.getString(TITLE_FILM,DEFF_FILM))
        txSub.setTextSize(preferences.getInt(FONT_SUBTITLE,DEF_FONT_SIZE).toFloat())
        txIsi.setText(preferences.getString(CONTENT_FILM,DEFF_CONTENT_FILM))
        txIsi.setTextSize(preferences.getInt(FONT_DETAIL,DEF_FONT_SIZE).toFloat())
        Toast.makeText(this,"Perubahan telah disimpan.",Toast.LENGTH_SHORT).show()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSet->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)

            }
        }
        return super.onOptionsItemSelected(item)
    }
    fun getBackground(myString: String, el:Boolean){
        if(myString.equals("Green")){
            if (el){
                constraintLayout.setBackgroundColor(Color.GREEN)
                txTitle.setTextColor(Color.BLACK)
                txSub.setTextColor(Color.BLACK)
                txIsi.setTextColor(Color.BLACK)
            }else{
                txHeader.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Blue")){
            if (el){
                constraintLayout.setBackgroundColor(Color.BLUE)
                txTitle.setTextColor(Color.WHITE)
                txSub.setTextColor(Color.WHITE)
                txIsi.setTextColor(Color.WHITE)
            }else{
                txHeader.setTextColor(Color.WHITE)
            }
        }
        if(myString.equals("Yellow")){
            if (el){
                constraintLayout.setBackgroundColor(Color.YELLOW)
                txTitle.setTextColor(Color.BLACK)
                txSub.setTextColor(Color.BLACK)
                txIsi.setTextColor(Color.BLACK)
            }else{
                txHeader.setTextColor(Color.BLACK)
            }
        }
        if(myString.equals("Black")){
            if (el){
                constraintLayout.setBackgroundColor(Color.BLACK)
                txTitle.setTextColor(Color.WHITE)
                txSub.setTextColor(Color.WHITE)
                txIsi.setTextColor(Color.WHITE)
            }else{
                txHeader.setTextColor(Color.WHITE)
            }
        }
    }

}

